<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TableController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});

Route::controller(UserController::class)->group(function () {
	Route::get('register', 'register')->name('register');
	Route::post('register', 'registerSimpan')->name('register.simpan');

	Route::get('login', 'login')->name('login');
	Route::post('login', 'loginAksi')->name('login.aksi');

	Route::get('logout', 'logout')->middleware('auth')->name('logout');
});


Route::middleware('auth')->group(function () {

	
Route::controller(CastController::class)->prefix('casts')->group(function () {
    Route::get('', 'index')->name('casts');
    Route::get('tambah', 'tambah')->name('casts.tambah');
    Route::post('tambah', 'simpan')->name('casts.tambah.simpan');
    Route::get('edit/{id}', 'edit')->name('casts.edit');
    Route::post('edit/{id}', 'update')->name('casts.tambah.update');
    Route::get('hapus/{id}', 'hapus')->name('casts.hapus');
});
});

Route::get('/data-tables', [TableController::class,'index']);

